package com.truckla.cars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class CarsApplication {
	public static void main(String[] args) {
		SpringApplication.run(CarsApplication.class, args);
	}

}
